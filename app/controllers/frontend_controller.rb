class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
  before_filter :set_subscribe
	
  def index
  	@posts = KepplerBlog::Post.where(public: true).first(5)
    @user_elis = User.find(2)
    @user_aure = User.find(3)
  	@categories = KepplerBlog::Category.all
  end

  def about_user
    @user = User.find_by_permalink(params[:user_permalink])
    @posts = KepplerBlog::Post.where(user_id: @user.id).all
  end

  def welcome
    @user = User.all
  end

  private

  def set_subscribe
    @subscribe = Subscribe.new
  end

  
end
