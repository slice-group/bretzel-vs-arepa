#Generado con Keppler.
class SubscribesController < ApplicationController  
  before_filter :authenticate_user!, except: [:create]
  layout 'admin/application'
  load_and_authorize_resource except: [:create]
  before_action :set_subscribe, only: [:show, :edit, :update, :destroy]

  # GET /subscribes
  def index
    subscribes = Subscribe.searching(@query).all
    @objects, @total = subscribes.page(@current_page), subscribes.size
    redirect_to subscribes_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end


  # POST /subscribes
  def create
    @subscribe = Subscribe.new(subscribe_params)
    @subscribe.save

    respond_to do |format|
      format.js
    end
  end

  # DELETE /subscribes/1
  def destroy
    @subscribe.destroy
    redirect_to subscribes_url, notice: 'Subscribe was successfully destroyed.'
  end

  def destroy_multiple
    Subscribe.destroy redefine_ids(params[:multiple_ids])
    redirect_to subscribes_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscribe
      @subscribe = Subscribe.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subscribe_params
      params.require(:subscribe).permit(:email)
    end
end
